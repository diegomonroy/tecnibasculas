# WordPress Theme

Custom base theme for WordPress.

```bash
# In our development directory we initialize Git and configure it
$ git init
$ git remote add origin ssh://git@bitbucket.org/diegomonroy/wordpress-theme.git
$ git config user.name "User Names"
$ git config user.email user@email.com
$ git config -l

# Install globally Gulp.js
$ sudo npm i -g gulp

# Install globally Bower
$ sudo npm i -g bower

# Do `pull` to the repository
$ git pull origin master

# Download the project dependencies contained in our package.json file
$ npm install

# Download the project dependencies contained in our bower.json file
$ bower install
```